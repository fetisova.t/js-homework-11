/**
 * Created on 04.07.2019.
 */
//Почему для работы с input не рекомендуется использовать события клавиатуры?
    // Для Input не рекомендуется использовать события клавиатуры, так как пользователь может использовать другой вариант ввода инормации, например, рукописный ввод с планшета или голосовой ввод, в таком случае события клавишь не сработают
let buttons = document.querySelectorAll('.btn');

document.addEventListener('keyup',(event)=>{
    let targetButton = event.keyCode;
    buttons.forEach(function(elem){
        if(+elem.dataset.keycode === targetButton){
            elem.classList.add('active');
        }else{
            elem.classList.remove('active')
        }
    })

});

